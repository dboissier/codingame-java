package org.codinjutsu.codingame.marslander;

class MarsLander {

    private static final int VERTICAL_SPEED_LIMIT = -40;

    static int computeBreakingPower(int currentVSpeed, int currentPower) {
        if (currentVSpeed < VERTICAL_SPEED_LIMIT) {
            return (currentPower < 4) ? currentPower + 1 : currentPower;
        } else if (currentVSpeed > VERTICAL_SPEED_LIMIT) {
            return (currentPower > 0) ? currentPower - 1 : currentPower;
        }
        return currentPower;
    }
}
