package org.codinjutsu.codingame.thor;

class Thor {

    static Move evaluate(int thorX, int thorY, int lightX, int lightY) {
        int vectorX = thorX - lightX;
        int vectorY = thorY - lightY;

        double arcTangent = (vectorY == 0) ? 90.0 : Math.abs(Math.toDegrees(Math.atan(vectorX / vectorY)));

        int nextPosX = computeNextPosition(vectorX, thorX);
        int nextPosY = computeNextPosition(vectorY, thorY);

        String firstCoordinate = computeCoordinate(vectorY, "S", "N");
        String secondCoordinate = computeCoordinate(vectorX, "E", "W");

        return createMove(thorX, thorY, nextPosX, nextPosY, arcTangent, firstCoordinate, secondCoordinate);
    }

    private static Move createMove(int thorX, int thorY, int nextPosX, int nextPosY, double arcTangent, String firstCoordinate, String secondCoordinate) {
        if (arcTangent < 45) {
            return new Move(firstCoordinate, thorX, nextPosY);
        } else if (arcTangent > 45) {
            return new Move(secondCoordinate, nextPosX, thorY);
        } else {
            return new Move(firstCoordinate + secondCoordinate, nextPosX, nextPosY);
        }
    }

    private static int computeNextPosition(int vector, int position) {
        if (vector == 0) {
            return position;
        } else if (vector < 0) {
            return position + 1;
        } else {
            return position - 1;
        }
    }

    private static String computeCoordinate(int vector, String whenVectorIsNegative, String whenVectorIsPositive) {
        return vector < 0 ? whenVectorIsNegative : whenVectorIsPositive;
    }

    static class Move {

        final String coordinates;

        final int nextPosX;

        final int nextPosY;

        Move(String coordinates, int nextPosX, int nextPosY) {
            this.coordinates = coordinates;
            this.nextPosX = nextPosX;
            this.nextPosY = nextPosY;
        }
    }
}
