package org.codinjutsu.codingame.nspoon;


class PowerNode {

    private static final String NO_NEIGHBORS = " -1 -1";

    static void addNodes(String nodesToAdd, Boolean[][] nodeMatrix, int position) {
        String[] nodes = nodesToAdd.split("");
        for (int i = 0, nodesLength = nodes.length; i < nodesLength; i++) {
            Boolean isNodePresent = "0".equals(nodes[i]) ? Boolean.TRUE : Boolean.FALSE;
            nodeMatrix[position][i] = isNodePresent;
        }
    }

    static String neighbors(Boolean[][] nodeMatrix, int width, int height, int nodeX, int nodeY) {
        return formatNodeCoordinates(nodeX, nodeY)
                + findRightNeighbor(nodeMatrix, width, nodeX, nodeY)
                + findBottomNeighbor(nodeMatrix, height, nodeX, nodeY);
    }

    private static String findRightNeighbor(Boolean[][] nodeMatrix, int width, int nodeX, int nodeY) {
        int neighborX = nodeX + 1;
        if (neighborX >= width) {
            return NO_NEIGHBORS;
        }
        if (isPresentNode(nodeMatrix, neighborX, nodeY)) {
            return formatNodeNeighbor(neighborX, nodeY);
        } else {
            return findRightNeighbor(nodeMatrix, width, neighborX, nodeY);
        }
    }

    private static String findBottomNeighbor(Boolean[][] nodeMatrix, int height, int nodeX, int nodeY) {
        int neighborY = nodeY + 1;
        if (neighborY >= height) {
            return NO_NEIGHBORS;
        }
        if (isPresentNode(nodeMatrix, nodeX, neighborY)) {
            return formatNodeNeighbor(nodeX, neighborY);
        } else {
            return findBottomNeighbor(nodeMatrix, height, nodeX, neighborY);
        }
    }

    private static boolean isPresentNode(Boolean[][] nodeMatrix, int neighborX, int neighborY) {
        return nodeMatrix[neighborY][neighborX];
    }

    private static String formatNodeCoordinates(int nodeX, int nodeY) {
        return String.format("%s %s", nodeX, nodeY);
    }

    private static String formatNodeNeighbor(int neighborX, int neighborY) {
        return String.format(" %s %s", neighborX, neighborY);
    }
}
