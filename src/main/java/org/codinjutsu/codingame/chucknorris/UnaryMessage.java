package org.codinjutsu.codingame.chucknorris;

class UnaryMessage {

    private static final int REQUIRED_BIT_LENGTH_OF_CHAR = 7;

    static String encodeToUnaryMessage(String string) {
        return encodeBinaryString(toBinaryString(string));
    }

    static String toBinaryString(String message) {
        StringBuilder stringBuilder = new StringBuilder();
        for (char aChar : message.toCharArray()) {
            stringBuilder.append(leftPadIfNecessary(Integer.toBinaryString(aChar)));
        }
        return stringBuilder.toString();
    }

    private static String leftPadIfNecessary(String string) {
        int diff = REQUIRED_BIT_LENGTH_OF_CHAR - string.length();
        if (diff > 0) {
            return repeatZero(diff) + string;
        }
        return string;
    }

    static String encodeBinaryString(String binaryString) {
        StringBuilder stringBuilder = new StringBuilder();
        char[] charArray = binaryString.toCharArray();
        char currentChar = charArray[0];
        int numberOfCurrentChar = 1;
        for (int i = 1, charArrayLength = charArray.length; i < charArrayLength; i++) {
            char aChar = charArray[i];
            if (aChar != currentChar) {
                stringBuilder
                        .append(format(currentChar, numberOfCurrentChar))
                        .append(" ");
                numberOfCurrentChar = 1;
            } else {
                numberOfCurrentChar++;
            }
            currentChar = aChar;
        }
        return stringBuilder.append(format(currentChar, numberOfCurrentChar)).toString();
    }

    static String format(char firstBlockChar, int numberOfSecondBlockChar) {
        String firstBlock = (firstBlockChar == '0') ? "00" : "0";
        return String.format("%s %s", firstBlock, repeatZero(numberOfSecondBlockChar));
    }

    private static String repeatZero(int repeatNumber) {
        StringBuilder buf = new StringBuilder(repeatNumber);
        for (int i = 0; i < repeatNumber; i++) {
            buf.append('0');
        }
        return buf.toString();
    }
}
