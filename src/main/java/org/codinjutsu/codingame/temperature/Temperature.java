package org.codinjutsu.codingame.temperature;

import java.util.StringTokenizer;

class Temperature {

    private static final int MAX_TEMPERATURE = 5526;

    static int findClosestToZero(String temperatures) {
        if ("".equals(temperatures)) {
            return 0;
        }

        StringTokenizer tokenizer = new StringTokenizer(temperatures);
        int closestToZeroTemperature = MAX_TEMPERATURE;
        while (tokenizer.hasMoreTokens()) {
            String temperatureStr = tokenizer.nextToken();
            Integer temperature = Integer.valueOf(temperatureStr);

            int temperatureInAbsoluteToAnalyze = Math.abs(temperature);
            int closestToZeroTemperatureInAbsoluteToAnalyze = Math.abs(closestToZeroTemperature);
            if (temperature != closestToZeroTemperature) {
                if (temperatureInAbsoluteToAnalyze == closestToZeroTemperatureInAbsoluteToAnalyze) {
                    closestToZeroTemperature = closestToZeroTemperatureInAbsoluteToAnalyze;
                } else if (temperatureInAbsoluteToAnalyze < closestToZeroTemperatureInAbsoluteToAnalyze) {
                    closestToZeroTemperature = temperature;
                }
            }
        }

        return closestToZeroTemperature;
    }
}
