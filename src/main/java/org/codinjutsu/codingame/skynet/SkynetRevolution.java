package org.codinjutsu.codingame.skynet;

import java.util.*;

class SkynetRevolution {

    static String findLinkToCut(int skynetNode, Network network) {
        DijkstraAlgorithm dijkstraAlgorithm = new DijkstraAlgorithm(network);
        dijkstraAlgorithm.execute(skynetNode);
        List<Integer> shortestPath = null;
        for (Integer gatewayNode : network.getGatewayNodes()) {
            List<Integer> path = dijkstraAlgorithm.getPath(gatewayNode);
            if (shortestPath == null || shortestPath.size() > path.size()) {
                shortestPath = path;
            }
        }
        if (shortestPath == null || shortestPath.size() < 2) {
            throw new IllegalStateException(String.format("unable to find path from %s and the gateways %s", skynetNode, network.getGatewayNodes()));
        }

        cutLink(network, shortestPath.get(0), shortestPath.get(1));

        return String.format("%s %s", shortestPath.get(0), shortestPath.get(1));
    }

    private static void cutLink(Network network, Integer srcNode, Integer destNode) {
        List<Link> links = network.getLinks();
        Iterator<Link> iterator = links.iterator();
        Link linkToRemove = null;
        while (iterator.hasNext()) {
            Link link = iterator.next();
            if (link.getDestNode() == srcNode && link.getSrcNode() == destNode) {
                linkToRemove = link;
            }
        }
        if (linkToRemove != null) {
            links.remove(linkToRemove);
        }
    }


    static class DijkstraAlgorithm {

        private static final int LINK_WEIGHT = 1;

        private final List<Link> links;
        private Set<Integer> visitedNodes;
        private Set<Integer> unSettledNodes;
        private Map<Integer, Integer> predecessorNodes;
        private Map<Integer, Integer> distanceByNode;

        DijkstraAlgorithm(Network network) {
            this.links = new ArrayList<>(network.getLinks());
        }

        void execute(Integer sourceNode) {
            visitedNodes = new HashSet<>();
            unSettledNodes = new HashSet<>();
            distanceByNode = new HashMap<>();
            predecessorNodes = new HashMap<>();
            distanceByNode.put(sourceNode, 0);
            unSettledNodes.add(sourceNode);
            while (!unSettledNodes.isEmpty()) {
                Integer nodeWithMinimalDistance = getMinimum(unSettledNodes);
                visitedNodes.add(nodeWithMinimalDistance);
                unSettledNodes.remove(nodeWithMinimalDistance);
                findMinimalDistances(nodeWithMinimalDistance);
            }
        }

        private void findMinimalDistances(Integer node) {
            List<Integer> neighborNodes = getNeighbors(node);
            for (Integer neighborNode : neighborNodes) {
                int shortestDistance = getShortestDistance(node);
                int distance = getDistance(node, neighborNode);
                if (getShortestDistance(neighborNode) > shortestDistance + distance) {
                    distanceByNode.put(neighborNode, shortestDistance + distance);
                    predecessorNodes.put(neighborNode, node);
                    unSettledNodes.add(neighborNode);
                }
            }
        }

        private int getDistance(Integer srcNode, Integer destNode) {
            for (Link link : links) {
                if (link.getSrcNode() == srcNode && link.getDestNode() == destNode) {
                    return LINK_WEIGHT;
                }
            }
            throw new IllegalStateException("Link list is incorrect, should review graph decomposition");
        }

        private List<Integer> getNeighbors(int srcNode) {
            List<Integer> neighbors = new ArrayList<>();
            for (Link link : links) {
                if (link.getSrcNode() == srcNode && !isVisited(link.getDestNode())) {
                    neighbors.add(link.getDestNode());
                }
            }
            return neighbors;
        }

        private Integer getMinimum(Set<Integer> nodes) {
            Integer minimum = null;
            for (Integer node : nodes) {
                if (minimum == null || getShortestDistance(node) < getShortestDistance(minimum)) {
                    minimum = node;
                }
            }
            return minimum;
        }

        private boolean isVisited(int node) {
            return visitedNodes.contains(node);
        }

        private int getShortestDistance(Integer destination) {
            Integer distance = distanceByNode.get(destination);
            if (distance == null) {
                return Integer.MAX_VALUE;
            } else {
                return distance;
            }
        }

        List<Integer> getPath(int destNode) {
            List<Integer> path = new ArrayList<>();
            Integer stepNode = destNode;
            if (predecessorNodes.get(stepNode) == null) {
                return null;
            }
            path.add(stepNode);
            while (predecessorNodes.get(stepNode) != null) {
                stepNode = predecessorNodes.get(stepNode);
                path.add(stepNode);
            }
            return path;
        }
    }

    static class NetworkBuilder {

        private final List<Link> links;
        private final List<Integer> gatewayNodes = new ArrayList<>();

        NetworkBuilder(int numberOfNodes) {
            links = new ArrayList<>(numberOfNodes);
        }

        NetworkBuilder addLink(int srcNode, int destNode) {
            links.add(new Link(srcNode, destNode));
            return this;
        }

        Network build() {
            return new Network(gatewayNodes, links);
        }

        NetworkBuilder addGatewayNode(int gatewayNode) {
            gatewayNodes.add(gatewayNode);
            return this;
        }
    }

    static class Network {
        private List<Integer> nodes;
        private final List<Link> links;

        Network(List<Integer> nodes, List<Link> links) {
            this.nodes = nodes;
            this.links = links;
        }

        List<Link> getLinks() {
            return links;
        }

        List<Integer> getGatewayNodes() {
            return nodes;
        }
    }

    static class Link {
        private final int srcNode;
        private final int destNode;

        Link(int srcNode, int destNode) {
            this.srcNode = srcNode;
            this.destNode = destNode;
        }

        int getDestNode() {
            return destNode;
        }

        int getSrcNode() {
            return srcNode;
        }

        @Override
        public String toString() {
            return "(" + srcNode + ", " + destNode + ")";
        }
    }
}
