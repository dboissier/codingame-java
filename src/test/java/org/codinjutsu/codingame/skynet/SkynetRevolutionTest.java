package org.codinjutsu.codingame.skynet;

import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class SkynetRevolutionTest {
    @Test
    public void simpleTest() throws Exception {
        SkynetRevolution.Network network = new SkynetRevolution.NetworkBuilder(3)
                .addLink(1, 2)
                .addLink(1, 0)
                .addGatewayNode(2)
                .build();

        assertThat(SkynetRevolution.findLinkToCut(1, network), equalTo("2 1"));
    }

    @Test
    public void severalPath() throws Exception {
        SkynetRevolution.Network network = new SkynetRevolution.NetworkBuilder(4)
                .addLink(1, 3)
                .addLink(2, 3)
                .addLink(0, 1)
                .addLink(0, 2)
                .addGatewayNode(3)
                .build();

        assertThat(SkynetRevolution.findLinkToCut(0, network), equalTo("3 1"));
        assertThat(SkynetRevolution.findLinkToCut(2, network), equalTo("3 2"));
    }

    @Test
    @Ignore
    public void star() throws Exception {
        SkynetRevolution.Network network = new SkynetRevolution.NetworkBuilder(12)
                .addLink(0, 1)
                .addLink(0, 2)
                .addLink(0, 3)
                .addLink(0, 4)
                .addLink(0, 6)
                .addLink(0, 5)
                .addLink(0, 7)
                .addLink(0, 8)
                .addLink(0, 9)
                .addLink(0, 10)

                .addLink(1, 2)
                .addLink(2, 3)
                .addLink(3, 4)
                .addLink(4, 5)
                .addLink(5, 6)
                .addLink(6, 7)
                .addLink(7, 8)
                .addLink(8, 9)
                .addLink(9, 10)
                .addLink(10, 1)

                .addLink(11, 6)
                .addLink(11, 5)
                .addLink(11, 7)
                .addGatewayNode(0)
                .build();

        assertThat(SkynetRevolution.findLinkToCut(11, network), equalTo("5 0"));
    }

    @Test
    @Ignore
    public void tripleStar() throws Exception {
        SkynetRevolution.Network network = new SkynetRevolution.NetworkBuilder(38)
                .addLink(28, 36)
                .addLink(0, 1)
                .addLink(0, 2)
                .addLink(0, 3)
                .addLink(0, 4)
                .addLink(0, 5)
                .addLink(0, 9)
                .addLink(0, 6)
                .addLink(0, 7)
                .addLink(0, 8)
                .addLink(0, 10)
                .addLink(0, 11)
                .addLink(0, 12)
                .addLink(0, 13)
                .addLink(0, 14)
                .addLink(0, 15)
                .addLink(0, 16)
                .addLink(0, 17)

                .addLink(18, 19)
                .addLink(18, 20)
                .addLink(18, 21)
                .addLink(18, 22)
                .addLink(18, 23)
                .addLink(18, 24)
                .addLink(18, 25)
                .addLink(18, 27)

                .addLink(28, 29)
                .addLink(28, 30)
                .addLink(28, 31)
                .addLink(28, 32)
                .addLink(28, 33)
                .addLink(28, 34)
                .addLink(28, 35)

                .addLink(1, 2)
                .addLink(2, 3)
                .addLink(3, 4)
                .addLink(4, 5)
                .addLink(5, 6)
                .addLink(7, 8)
                .addLink(8, 9)
                .addLink(9, 10)
                .addLink(10, 11)
                .addLink(11, 12)
                .addLink(12, 13)
                .addLink(13, 14)
                .addLink(14, 15)
                .addLink(15, 16)
                .addLink(16, 17)
                .addLink(17, 1)

                .addLink(19, 20)
                .addLink(20, 21)
                .addLink(21, 22)
                .addLink(22, 23)
                .addLink(23, 24)
                .addLink(24, 25)
                .addLink(26, 27)
                .addLink(25, 26)
                .addLink(27, 19)

                .addLink(29, 30)
                .addLink(30, 31)
                .addLink(31, 32)
                .addLink(32, 33)
                .addLink(33, 34)
                .addLink(34, 35)
                .addLink(35, 23)
                .addLink(35, 36)

                .addLink(29, 21)
                .addLink(37, 35)
                .addLink(3, 34)
                .addLink(6, 7)
                .addLink(4, 33)
                .addLink(18, 26)
                .addLink(35, 2)
                .addLink(36, 22)
                .addLink(37, 1)
                .addLink(37, 2)
                .addLink(37, 23)
                .addLink(37, 24)
                .addLink(36, 29)

                .addGatewayNode(0)
                .addGatewayNode(18)
                .addGatewayNode(28)
                .build();

        assertThat(SkynetRevolution.findLinkToCut(37, network), equalTo("0 1"));
        assertThat(SkynetRevolution.findLinkToCut(35, network), equalTo("28 35"));
        assertThat(SkynetRevolution.findLinkToCut(2, network), equalTo("0 2"));
    }
}