package org.codinjutsu.codingame.chucknorris;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class UnaryMessageTest {

    @Test
    public void encode_C_Character() throws Exception {
        assertThat(
                UnaryMessage.encodeToUnaryMessage("C"),
                equalTo("0 0 00 0000 0 00"));
    }

    @Test
    public void encode_CC_Charracters() throws Exception {
        assertThat(
                UnaryMessage.encodeToUnaryMessage("CC"),
                equalTo("0 0 00 0000 0 000 00 0000 0 00"));
    }

    @Test
    public void encode_Modulo_Character() throws Exception {
        assertThat(
                UnaryMessage.encodeToUnaryMessage("%"),
                equalTo("00 0 0 0 00 00 0 0 00 0 0 0"));
    }

    @Test
    public void convert_C_ToBinaryString() throws Exception {
        assertThat(UnaryMessage.toBinaryString("C"), equalTo("1000011"));
    }

    @Test
    public void convert_Modulo_ToBinaryString() throws Exception {
        assertThat(UnaryMessage.toBinaryString("%"), equalTo("0100101"));
    }

    @Test
    public void convert_CC_ToBinaryString() throws Exception {
        assertThat(UnaryMessage.toBinaryString("CC"), equalTo("10000111000011"));
    }

    @Test
    public void convert_CModulo_ToBinaryString() throws Exception {
        assertThat(UnaryMessage.toBinaryString("C%"), equalTo("10000110100101"));
    }

    @Test
    public void encodeBinaryString() throws Exception {
        assertThat(UnaryMessage.encodeBinaryString("1000011"), equalTo("0 0 00 0000 0 00"));
    }

    @Test
    public void formatUnaryBlockMessage() throws Exception {
        assertThat(UnaryMessage.format('0', 4), equalTo("00 0000"));
        assertThat(UnaryMessage.format('1', 2), equalTo("0 00"));
    }
}