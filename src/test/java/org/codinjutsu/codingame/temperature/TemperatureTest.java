package org.codinjutsu.codingame.temperature;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TemperatureTest {

    @Test
    public void findClosestToZeroFromEmptyDataList() throws Exception {
        assertEquals(0, Temperature.findClosestToZero(""));
    }

    @Test
    public void findClosestToZeroFromSimpleDataList() throws Exception {
        assertEquals(1, Temperature.findClosestToZero("1 -2 -8 4 5"));
    }

    @Test
    public void findClosestToZeroWhen2NumbersHaveSameAbsoluteValue() throws Exception {
        assertEquals(2, Temperature.findClosestToZero("-2 2"));
    }
    @Test
    public void findClosestToZeroWhen2NegativeNumbersHaveSameValue() throws Exception {
        assertEquals(-10, Temperature.findClosestToZero("-10 20 -10"));
    }
}