package org.codinjutsu.codingame.nspoon;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class PowerNodeTest {

    @Test
    public void addSingleLineToNodeMatrix() throws Exception {
        Boolean[][] nodeMatrix = new Boolean[1][5];

        PowerNode.addNodes("0.0.0", nodeMatrix, 0);

        assertThat(nodeMatrix, equalTo(new Boolean[][]{
                {Boolean.TRUE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.TRUE}
        }));
    }

    @Test
    public void add2LinesToNodeMatrix() throws Exception {
        Boolean[][] nodeMatrix = new Boolean[2][2];

        PowerNode.addNodes("00", nodeMatrix, 0);
        PowerNode.addNodes("0.", nodeMatrix, 1);

        assertThat(nodeMatrix, equalTo(new Boolean[][]{
                {Boolean.TRUE, Boolean.TRUE},
                {Boolean.TRUE, Boolean.FALSE}
        }));
    }

    @Test
    public void neighborsIn2x2Matrix() throws Exception {
        Boolean[][] nodeMatrix = new Boolean[][]{
                {Boolean.TRUE, Boolean.TRUE},
                {Boolean.TRUE, Boolean.FALSE}
        };
        assertThat(PowerNode.neighbors(nodeMatrix, 2, 2, 0, 0), equalTo("0 0 1 0 0 1"));
        assertThat(PowerNode.neighbors(nodeMatrix, 2, 2, 1, 0), equalTo("1 0 -1 -1 -1 -1"));
        assertThat(PowerNode.neighbors(nodeMatrix, 2, 2, 1, 1), equalTo("1 1 -1 -1 -1 -1"));
    }

    @Test
    public void neighborsIn1x5Matrix() throws Exception {
        Boolean[][] nodeMatrix = new Boolean[][]{
                {Boolean.TRUE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.TRUE}
        };
        assertThat(PowerNode.neighbors(nodeMatrix, 5, 1, 0, 0), equalTo("0 0 2 0 -1 -1"));
        assertThat(PowerNode.neighbors(nodeMatrix, 5, 1, 1, 0), equalTo("1 0 2 0 -1 -1"));
        assertThat(PowerNode.neighbors(nodeMatrix, 5, 1, 2, 0), equalTo("2 0 4 0 -1 -1"));
        assertThat(PowerNode.neighbors(nodeMatrix, 5, 1, 3, 0), equalTo("3 0 4 0 -1 -1"));
        assertThat(PowerNode.neighbors(nodeMatrix, 5, 1, 4, 0), equalTo("4 0 -1 -1 -1 -1"));
    }

    @Test
    public void neighborsIn5x1Matrix() throws Exception {
        Boolean[][] nodeMatrix = new Boolean[][]{
                {Boolean.TRUE},
                {Boolean.FALSE},
                {Boolean.TRUE},
                {Boolean.FALSE},
                {Boolean.TRUE}
        };
        assertThat(PowerNode.neighbors(nodeMatrix, 1, 5, 0, 0), equalTo("0 0 -1 -1 0 2"));
        assertThat(PowerNode.neighbors(nodeMatrix, 1, 5, 0, 1), equalTo("0 1 -1 -1 0 2"));
        assertThat(PowerNode.neighbors(nodeMatrix, 1, 5, 0, 2), equalTo("0 2 -1 -1 0 4"));
        assertThat(PowerNode.neighbors(nodeMatrix, 1, 5, 0, 3), equalTo("0 3 -1 -1 0 4"));
        assertThat(PowerNode.neighbors(nodeMatrix, 1, 5, 0, 4), equalTo("0 4 -1 -1 -1 -1"));
    }
}