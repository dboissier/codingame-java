package org.codinjutsu.codingame.thor;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ThorTest {


    @Test
    public void moveToE() throws Exception {
        Thor.Move nextMove = Thor.evaluate(0, 0, 30, 0);
        assertEquals("E", nextMove.coordinates);
        assertEquals(1, nextMove.nextPosX);
        assertEquals(0, nextMove.nextPosY);

        assertEquals("E", Thor.evaluate(0, 15, 30, 30).coordinates);
        assertEquals("E", Thor.evaluate(0, 15, 30, 0).coordinates);
    }

    @Test
    public void moveToW() throws Exception {
        Thor.Move nextMove = Thor.evaluate(30, 0, 0, 0);
        assertEquals("W", nextMove.coordinates);
        assertEquals(29, nextMove.nextPosX);
        assertEquals(0, nextMove.nextPosY);

        assertEquals("W", Thor.evaluate(30, 15, 0, 30).coordinates);
        assertEquals("W", Thor.evaluate(30, 15, 0, 0).coordinates);
    }

    @Test
    public void moveToN() throws Exception {
        Thor.Move nextMove = Thor.evaluate(30, 30, 30, 0);
        assertEquals("N", nextMove.coordinates);
        assertEquals(30, nextMove.nextPosX);
        assertEquals(29, nextMove.nextPosY);

        assertEquals("N", Thor.evaluate(15, 30, 30, 0).coordinates);
        assertEquals("N", Thor.evaluate(15, 30, 0, 0).coordinates);
    }

    @Test
    public void moveToS() throws Exception {
        Thor.Move nextMove = Thor.evaluate(30, 0, 30, 30);
        assertEquals("S", nextMove.coordinates);
        assertEquals(30, nextMove.nextPosX);
        assertEquals(1, nextMove.nextPosY);

        assertEquals("S", Thor.evaluate(15, 0, 30, 30).coordinates);
        assertEquals("S", Thor.evaluate(15, 0, 0, 30).coordinates);
    }


    @Test
    public void moveToSW() throws Exception {
        Thor.Move nextMove = Thor.evaluate(30, 0, 0, 30);
        assertEquals("SW", nextMove.coordinates);
        assertEquals(29, nextMove.nextPosX);
        assertEquals(1, nextMove.nextPosY);
    }

    @Test
    public void moveToSE() throws Exception {
        Thor.Move nextMove = Thor.evaluate(0, 0, 30, 30);
        assertEquals("SE", nextMove.coordinates);
        assertEquals(1, nextMove.nextPosX);
        assertEquals(1, nextMove.nextPosY);
    }

    @Test
    public void moveToNE() throws Exception {
        Thor.Move nextMove = Thor.evaluate(0, 30, 30, 0);
        assertEquals("NE", nextMove.coordinates);
        assertEquals(1, nextMove.nextPosX);
        assertEquals(29, nextMove.nextPosY);
    }

    @Test
    public void moveToNW() throws Exception {
        Thor.Move nextMove = Thor.evaluate(30, 30, 0, 0);
        assertEquals("NW", nextMove.coordinates);
        assertEquals(29, nextMove.nextPosX);
        assertEquals(29, nextMove.nextPosY);
    }
}
