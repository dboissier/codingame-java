package org.codinjutsu.codingame.marslander;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class MarsLanderTest {

    @Test
    public void computeBreakingPower() throws Exception {
        assertThat(MarsLander.computeBreakingPower(-45, 3), equalTo(4));
        assertThat(MarsLander.computeBreakingPower(-40, 3), equalTo(3));
        assertThat(MarsLander.computeBreakingPower(-39, 2), equalTo(1));
    }
}